// console.log(`Hello, JSON`);

/*
	JSON - Javascript Object Notation - it is a popular data format which applications use to communicate with each other.
	
	JSON - aptly named after JS Objects looks like a JavaScript
	Object with a {} and key-value pairs. However, the keys of a Json object is surrounded by "".

	{
		"key1": "ValueA",
		"key2": 25000,
	}
*/

let sampleJSON = `

	{
		"name": "Katniss Everdeen",
		"age": 20,
		"address": {
			"city": "Kansan City",
			"state": "Kansas"
		}
	}

`;

console.log(typeof sampleJSON);

//Are we able to turn a JSON into a JS object?
//JSON.parse() - will return the given JSON as a JS object.
//JSON -> JS Object  - JSON.parse()
let sampleObject1 = JSON.parse(sampleJSON);
console.log(sampleObject1);

//JSON.stringify() - will return a givn JS object as a stringified JSON format string.
//JS Object -> JSON - JSON.stringify()
let user1 = {
	username: "knight123",
	password: "1234",
	age: 25
}

let sampleStringified = JSON.stringify(user1);

console.log(sampleStringified);

//JSON Array
//Array of JSON in JSON format.

let sampleArr = `
	[
		{
			"email": "jasonDerulo@gmail.com",
			"password": "1234safe",
			"isAdmin": false
		},
		{
			"email": "jasondelapena@gmail.com",
			"password": "jayson086",
			"isAdmin": false
		}
	]
`;

console.log(sampleArr);
//to delete last item on the array
//first turn the JSON format array to a JS array
let parsedArr = JSON.parse(sampleArr);
console.log(parsedArr);

//delete the last item in the array using pop()
parsedArr.pop();
console.log(parsedArr);


//We can now turn the parsedArr back into JSON and update the sampleArr JSON array 
sampleArr = JSON.stringify(parsedArr);
console.log(sampleArr);

//Database (JSON Format) => server (JSON Format to JS Object) => process (Task to do the data) => turn the data back to JSON => client (Web Page)

//Dos and don'ts of JSON Format

//Do: add double quotes to your keys
//Do: add colon after each key
//Don't: don't add excessive commas
//Don't: don't forget to close your double quotes, curly brace
let sampleData = `

	{
		"email": "james123@gmail.com",
		"password": "1234james",
		"balance": 50000
	}

`;
//When you parse JSON with an erratic format, it produces an error.
let parsedData = JSON.parse(sampleData);
console.log(parsedData)

//Sublime text tip: to help check the format of your JSON you can change the linting from JavaScript to JSON.